$(function(){
    //Ie + Ie number
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
       $('body').addClass('ie').addClass('ie'+parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    }
    
});
validate = {
    init: function() {
        if ($('.parsleyform').length) {
            $('.parsleyform').each(function(e) {
                var thisi = $(this);
                thisi.parsley({
                    errors: {
                        classHandler: function(elem, isRadioOrCheckbox) {
                            //console.log($(elem));
                            if (isRadioOrCheckbox) {
                                return $(elem).closest('.checkbox_group');
                            }
                            else if($(elem).hasClass('selectbox')){
                                return $(elem).closest('.selectparent');
                            }
                        },
                        container: function(element, isRadioOrCheckbox) {
                            if (isRadioOrCheckbox) {
                                return element.closest('.checkbox_group');
                            }
                        }
                    }
                });
            });
        }
    },
    extended: function() {
        //* iCheck
        if ($('.icheck').length) {
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            }).on('ifChanged', function(event) {
                $('.icheck[name="check"]').parsley('validate');
            });
        }
    }
};
//* chackbox and radio buttons
icheck = {
    init: function() {
        if ($('.icheck').length) {
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            });
        }
    }
};
// Document Ready
$(document).ready(function() {
    icheck.init();
    validate.init();
    validate.extended();
    new WOW().init();
    /*$('.nav_table li').hover(function() {
        $(this).find('ul').stop(true,true).fadeIn();
        },function(){
        $(this).find('ul').stop(true,true).hide();
    });*/ 
    /*if( $('.fancybox-thumbs').length > 0)
    {
        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',
            padding : 0,
            closeBtn  : true,
            arrows    : true,
            nextClick : true,
            helpers : {
                thumbs : {
                    width  : 90,
                    height : 90
                }
            }
        });
    }*/
    /*if( $('.fancybox').length > 0){
        $(".fancybox").fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            padding : 0
        });
    }*/
    /*if( $('.fancybox-media').length > 0){
        $('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            padding : 0,
            helpers : {
                media : {}
            }
        });
    }*/
    /*if( $('#startpop').length > 0){
        $.fancybox.open('#startpop');
        //$("#startpop").fancybox().trigger('click');
    }*/
    if( $('.selectbox').length > 0)
    {
        $(".selectbox").selectbox();
    }
    $(".lazy").lazyload({
        event : "sporty",
        effect : "fadeIn",
    });
    $(window).bind("load", function() {
        if( $('.lazy').length > 0) {
            var timeout = setTimeout(function() { $(".lazy").trigger("sporty") }, 100);
        }
    });
    /*if( $('.slickslider').length > 0){
        $('.slickslider').slick({
            dots: false,
            autoplay: true,
            infinite: true,
            fade: false,
            speed: 400,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 7000,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        });
    }
    if( $('.slickcarousel').length > 0){
        $('.slickcarousel').slick({
            dots: true,
            speed: 300,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 3,
            slidesToScroll: 3,
            fade: false,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        }).on('afterChange', function(event, slick, direction){
            //console.log(direction);
            // left
        });
    }*/
    /*if( $('.slicksc').length > 0){
        $('.slicksc').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slickscc'
        });
        $('.slickscc').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            centerMode: true,
            focusOnSelect: true,
            asNavFor: '.slicksc'
        });
    }*/
    /*if( $('.resize-img').length > 0){
        $('.resize-img img').resizeToParent();    
    }*/
    /*$('#accordion h3 a').click(function(){
        window.location.replace($(this).attr('href'));
    });*/
	/*$('a.click').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 1000);
    });*/
    /* if (window.location.hash) {
        var url = window.location.hash;
        var url1= url.substr(1,50);
        $('.adofilter').find('a[href='+url1+']').trigger('click');
    }*/
    /*if ( $('.mCustomScrollbar').length > 0) {
        $('.mCustomScrollbar').mCustomScrollbar({
            mouseWheel:{
                scrollAmount: 300
            }
        }); 
    }*/
    /*if( $('.flexslidebanner .slides li').length < 2)
    {
        $('.flexslidebanner').addClass('nobuton');
    } */
    //windowHeight.init(window);
});

// Window Load
$(window).load(function() {
    /*if( $('.flexslider').length > 0)
    {
        $('.carousel').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            directionNav: true,
            itemWidth: 310,
            itemMargin: 5
            //asNavFor: '#slider'
        });
        $('.flexslide').flexslider({
            //smoothHeight: true,//animationLoop: true, //startAt: 0,//slideshow: true,
            //slideshowSpeed: 7000,//animationSpeed: 600,//touch: true,//video: false,
            //pauseOnAction: true,//pauseOnHover: false,//prevText: "Previous",//nextText: "Next",
            controlNav: false,
            directionNav: true,
            animation: "slide",
            slideshow: true,
            pauseOnAction: false,
            start: function(){
                $('.panel-collapse').removeClass('in');
                $('.panel:first-child .panel-collapse').addClass('in');
                $('.tab-pane').removeClass('active').removeClass('in');
                $('.tab-pane:first-child').addClass('active').addClass('in');
                var settimer = setTimeout(function(){
                    clearTimeout(settimer);
                }, 300);
            }
            //sync: "#carousel"
        }); 
    }*/
    /*if( $('.flexslidecon').length > 0)
    {
        $('.tab-pane').each(function(e) {
            var thisi = $(this);
            thisi.find('.carouseli').attr('id','carouseli'+e);
            thisi.find('.slideri').attr('id','slideri'+e);
            $('#carouseli'+e).flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                itemWidth: 71,
                itemMargin: 20,
                asNavFor: '#slideri'+e
            });
            $('#slideri'+e).flexslider({
                //smoothHeight: true,//animationLoop: true, //startAt: 0,//slideshow: true,
                //slideshowSpeed: 7000,//animationSpeed: 600,//touch: true,//video: false,
                //pauseOnAction: true,//pauseOnHover: false,//prevText: "Previous",//nextText: "Next",
                controlNav: false,
                directionNav: false,
                animation: "slide",
                slideshow: true,
                pauseOnAction: false,
                start: function(){
                    $('.panel-collapse').removeClass('in');
                    $('.panel:first-child .panel-collapse').addClass('in');
                    $('.tab-pane').removeClass('active').removeClass('in');
                    $('.tab-pane:first-child').addClass('active').addClass('in');
                },
                sync: '#carouseli'+e
            }); 
        });
    }*/
	if( !$('#supersized li').length > 0)
    {
        $("#supersized-loader").hide();
    } 
    if( $('#supersized li').length == 0)
    {
        $("#supersized").hide();
    } 
    /*if( $('.maxeight').length > 0){
        var maxHeight = 0;
        $('.maxeight').each(function() {
            if ($(this).height() > maxHeight)
                maxHeight = $(this).height();
        });
        $('.maxeight').each(function() {
            $(this).height(maxHeight);
        });
    }*/
	/*$('.linkaatarget').each(function() {
        $(this).find('a').not('.fancybox-thumbs, .fancybox, .fancybox-fade').attr('target', '_blank');
    });*/
    /*if( $('.').length > 0){}*/
    /*$('#loadingg').fadeOut();*/
    //windowHeight.init(window);
});

// Window Resize
$(window).resize(function(){
    //windowHeight.init(window);
});

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
/*windowHeight = {
    init: function(windowi){
        var windowHeight = $(windowi).outerHeight();
       
        if (windowHeight < 630) {
            var contentHeightbox = 630 - 200;
            $('#content').outerHeight(contentHeightbox);
        }else {

            $('#content').outerHeight(contentHeightbox);
        }
    }
}*/